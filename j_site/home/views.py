from django.http import response
from django.shortcuts import redirect, render, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import CreateUserForm

# Create your views here.
def loginpage(request):
    if request.user.is_authenticated: #checks if the user is alredy loged in and redirects
        return redirect('dash')
    else:
        if request.method=="POST":
            username=request.POST.get('uname')
            password=request.POST.get('pass')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('dash')
            else:
                messages.info(request, "username or password is incorrect")
                context={}
                return render(request, "login.html", context)
        return render(request,"login.html")

def signup(request):
    if request.user.is_authenticated:
        return redirect('dash')
    else:
        context={}
        form = CreateUserForm()
        context ={"form":form}

        if request.method == "POST":
            print(request.POST)
            form = CreateUserForm(request.POST)
            if form.is_valid():
                print("saving")
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, "account was created for "+ user)
                return redirect("loginpage")
            else:
                print(form.errors)
                print("failed")
                context ={"form":form,"errors":form.errors}
        return render(request,"signup.html",context)

def logoutuser(request):
    logout(request)
    return redirect("loginpage")

@login_required(login_url="loginpage")
def dash(request):
    # if 'device_form_submit' in request.POST:
    #     print(request.POST)
    # elif 'details_form_submit' in request.POST:
    #     print(request.POST)
    return render(request,"new_dash.html")